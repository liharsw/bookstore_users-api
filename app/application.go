package app

import (
	"github.com/gin-gonic/gin"
	"gitlab.com/liharsw/bookstore_users-api/logger"
)

var (
	router = gin.Default()
)

func StartApplication() {
	mapUrls()

	logger.Log.Info("We start our application dude!")

	router.Run(":8080")
}
