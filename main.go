package main

import "gitlab.com/liharsw/bookstore_users-api/app"

func main() {
	app.StartApplication()
}
